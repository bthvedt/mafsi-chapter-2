import './pipwerks_scorm.js'; // the scorm api wrapper. Adds a global object pipwerks.SCORM

var scorm = pipwerks.SCORM;  //Shortcut
var lmsConnected = false;
var learnername;
var completionstatus;
var lesson_location;

// scorm.version = "1.2";
scorm.version = '2004';

export default {
	connect() {
		//console.log('connect called');
		if (scorm.init()) {
			scorm.set('cmi.exit','suspend'); // have to do this in order to save stuff, apparently. WHATEVER
			return true;
		} else {
			return false;
		}
	},
	setVersion(version) {
		// can be 1.2, 2004 1st edition, or whatever
		console.log('setVersion called');
		scorm.version = version;
	},
	getVersion() {
		console.log(scorm.version);
	},
	bookmark(place) {
		scorm.set("cmi.location",place);
	},
	getBookmark() {
		return scorm.get("cmi.location");
	},
	suspendData(str) {
		scorm.set("cmi.suspend_data",str);
	},
	getSuspendedData() {
		return scorm.get("cmi.suspend_data");
	},
	recordQuestion(data) {
		// meh.. might have to re-write this for different types of quizzes

		let num = data.num;

		scorm.set("cmi.interactions."+ num +".id", data.num); // id
		scorm.set("cmi.interactions."+ num +".description",data.question); // question 

		// Damn .. was hoping question choices could be recorded
		//for (var x = 0; x < data.choices.length; x++) {  // choices .. cmi.interactions.n.objectives.n.id
		//	scorm.set("cmi.interactions."+ num +".objectives."+x+".id",data.choices[x]);
		//}

		scorm.set("cmi.interactions."+ num +".type", "other"); // type .. they're all "choice" for this one
		scorm.set("cmi.interactions."+ num +".learner_response", data.response); // response
		scorm.set("cmi.interactions."+ num +".result", "correct"); // result
	},
	setScore(min,max,score) {

		var scaled = (score/max);


		scorm.set("cmi.score.raw", score);
		scorm.set("cmi.score.max", max);
        scorm.set("cmi.score.min", min);
        scorm.set("cmi.score.scaled", scaled ); // have to set this.. yeah, it would be helpful to have MENTIONED THAT IT NEEDS TO BE SET IN THE SCORM DOCUMENTATION, WOULD HAVE SAVED ME A LITTLE TIME!!!!!!
	},
	setStatus(status) {

		scorm.set('cmi.success_status',status);
	},
	setCompletion(completionString) {

		scorm.set('cmi.completion_status',completionString);
	}
}