import lms from '../../lms/lmsHelper.js'
import { router } from '../../main.js'

// Right now this only handles 2004
// https://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/

const state = {
	version: '2004',
	connected: false,
	learnerName: '',
	complete: false,
	status: '',
	score: 0,
	quizHelper: function() {
		this.questionData = {};
		this.questionData.questions = {};

		this.setQuestionData = function(num,text,userAnswer,correctAnswer) {
			this.questionData.questions[num] = {
				text: text,
				userAnswer: userAnswer,
				correctAnswer: correctAnswer,
			}
		}

		this.getQuestionData = function(num) {
			return this.questionData.questions[num];
		}
	}
}

const getters = {
	getBookmarkedLocation() {

	},

	getScormVersion() {
		//lms.getVersion();
	},
	lmsConnected(state) {
		return state.connected;
	},
	getBookmark(state) {
		var location = lms.getBookmark();
		console.log('getting bookmark');

		if (location) {
			return location;
		} else {
			console.log('cant get bookmark');
		}
		
	},

	getSavedString() {
		var str = lms.getSuspendedData();

		if (str) {
			return str;
		} else {
			console.log('cant get saved string');
		}
	}

}  

const actions = {
	lmsConnect({commit}) {
		//lmsConnected = scorm.init();
		commit('setConnectionStatus',lms.connect());
	},

	setScormVersion({commit}, vers) {
		lms.setVersion(vers);
	},

	bookmark({commit},place) {
		if (state.connected) {
			lms.bookmark(place);
			return place;
		} else {
			console.log('cant bookmark, not connected');
			return false;
		}
	},

	saveString({commit},string) {
		lms.suspendData(string); // saves suspended data
	},
	recordQuestion({commit,state},data) {

		// data has question num, response, correct, and description

        if (state.connected) {
			lms.recordQuestion(data);
		} else {
			console.log('cant record question, not connected');
			return false;
		}

	},
	setScore({state},score) {
		lms.setScore(0,100,score);
	},
	setStatus({state},status) {
		// passed/failed
		lms.setStatus(status);
	},
	setCompletion({state},completionString) {
		// “completed”/“incomplete”, 
		lms.setCompletion(completionString);
	}

}

const mutations = {
	setConnectionStatus(state,connection) {
		state.connected = connection;
	},
	setVersion() {

	},
	setLearnerName() {

	},
	bookMark() {

	},
	setScore() {

	},
	setStatus() {

	},
	setCompletion() {

	}
}

export default {
	state,
	getters,
	actions, 
	mutations
}

// store bokmarking and completed sections
// via a stringified JSON object in I dunno.. cmi.location??
// or perhaps cmi.suspend_data ... actually that looks better
// completeted_sections = []

// alright.. first test connection
// then .. test bookmarking
// then .. test.. storing completed data
// then .. test.. question data
// (cmi interactions)