import lms from '../../lms/lmsHelper.js'
import { router } from '../../main.js'
import Vue from 'vue'

// close "Resources"
// update "Resources"

const state = {
	siteNav: { // contains objects for each thing in the site navigation
	},
	currentURLPath: '', // stores the store's current section
	currentInteraction: '',
	currentComponent: '',
	totalInteractions: 0, // keep track of total number of interactions
	pagePosition: 0, // the position of the 'page' in the interaction
	onLastPage: false,
	onFirstPage: true, // let's just initialize this to true
	sectionallyHidden: false, //
	sectionallyOverRiddenRight: false, // sections can override navigation with their own functionality
	rightFunction: null, // the callback function to execute IF navigation is overridden for right
	sectionallyOverRiddenLeft: false, // todo: duplicate functionality of overriding right for this
	leftFunction: null, // todo: duplicate functionality of overriding right for this
	pageOverRiddenLeft: false, // pages can also override the left/right
	pageOverRiddenRight: false,
	pageLeftFunction: null, // these take precidence over the sectional overrideds
	pageRightFunction: null,
	prevDisabled: false, // disables previous function
	nextDisabled: false, // disables next function
	hackyOverrideVariable: false, // used to go to next/prev even when overridden
	afterInteractionCompleteFunction: null, // runs after the final page in each section
	beforeEachInteractionFunction: null, // runs when an interaction starts
	beforeEachPageFunction: null
}


const getters = {
	getUserLocationString() {
		return state.currentInteraction + '_' + state.currentComponent;
	},
	getSiteNav: (state) => state.siteNav,
	getCurrentComponent(state) {
		return state.currentComponent;
	},
	nextDisabled(state) {
		// to have added completxity added later...
		//console.log(state.onLastPage);
		// ALSO let's just DISABLE IF LOCKED
		if (typeof state.siteNav['/' + state.currentInteraction] != 'undefined') {
			if ((state.onLastPage) || (state.nextDisabled) || (state.siteNav['/' + state.currentInteraction].locked)) {
				return true;
			}
		}

		return false;
	},
	prevDisabled(state) {
		// to have added complexity added later ..
		if ((state.onFirstPage) || (state.prevDisabled)) {
			return true; // it starts out like this
		}

		return false;
	},
	navHidden() {
		// various conditions that hide the nav
		if (state.sectionallyHidden) { // hidden just for the section
			return true;
		} else {
			return false;
		}
	},
	getCurrentMenuName(state) {
		if (typeof state.siteNav['/' + state.currentInteraction] != 'undefined') {
			return state.siteNav['/' + state.currentInteraction].menuName;
		} else {
			return null;
		}
	},
	getCurrentMenuItem(state) {
		if (typeof state.siteNav['/' + state.currentInteraction] != 'undefined') {
			return {
				name: state.siteNav['/' + state.currentInteraction].menuName,
				link: '/' + state.currentInteraction,
				done: state.siteNav['/' + state.currentInteraction].done,
				locked: state.siteNav['/' + state.currentInteraction].locked,
				bookMark: state.siteNav['/' + state.currentInteraction].bookMark,
				trackCompletion: state.siteNav['/' + state.currentInteraction].trackCompletion
			}
		} else {
			return null;
		}
	},
	/*getAllMenuNames(state) {
		var arr = [];

		console.log('MENU NAMES ARE');
		console.log(state.siteNav);
		for (var x in state.siteNav) {

			if (state.siteNav[x].menuName != '') {
				arr.push({
					name: state.siteNav[x].menuName,
					link: x,
					done: state.siteNav[x].done,
					locked: state.siteNav[x].locked,
					bookMark: state.siteNav[x].bookMark,
					trackCompletion: state.siteNav[x].trackCompletion
				});
			}
		}
		return arr;
	},*/
	getAllMenuNames: state => () => {
		var arr = [];

		//console.log('MENU NAMES ARE');
		//console.log(state.siteNav);
		for (var x in state.siteNav) {

			if (state.siteNav[x].menuName != '') {
				arr.push({
					name: state.siteNav[x].menuName,
					link: x,
					done: state.siteNav[x].done,
					locked: state.siteNav[x].locked,
					bookMark: state.siteNav[x].bookMark,
					trackCompletion: state.siteNav[x].trackCompletion
				});
			}
		}
		return arr;
	},
	getAllMenuItems: state => () => {
		var arr = [];
		for (var x in state.siteNav) {
			if (state.siteNav[x].menuName != '') {
				arr.push({
					name: state.siteNav[x].menuName,
					link: x,
					done: state.siteNav[x].done,
					locked: state.siteNav[x].locked
				});
			}
		}
		return arr;
	},
	getAllInteractions(state) {
		//return state.siteNav;
		var arr = [];
		for (var x in state.siteNav) {
			arr.push(x);
		}
		return arr;
	},
	getCurrentPage(state) {
		if (typeof state.siteNav['/' + state.currentInteraction] != 'undefined') {
			return state.siteNav['/' + state.currentInteraction].pages[state.pagePosition];
		}
	},
	getCurrentInteraction(state) {
		return state.currentInteraction;
	},
	getNextInteraction(state) {
		if (typeof state.siteNav['/' + state.currentInteraction] != 'undefined') {
			var num = parseInt(state.siteNav['/' + state.currentInteraction].num);

			for (var x in state.siteNav) {
				console.log(x);
				if (parseInt(state.siteNav[x].num) == (num+1)) {
					return x.replace('/','');
				}
			}
		}

		return null;
	},
	sectionDone: state => link => {
		return state.siteNav[link].done;
	},
	sectionLocked: state => link => {
		return state.siteNav[link].locked;
	},
	previousSectionsDone: state => link => {
		// returns true if all previous sections to the link are done
		// IT ONLY COUNTS IF TRACKCOMPLETION IS SET TO TRUE

		// this is kind of annoying because objects in javascript can be in random orders
		// luckily I have a property 'num' on every link in the sitenav object
		// wonder if there's a less obfuscatios way to do this
		var alldone = true;
		var sortArr = [];
		for (var x in state.siteNav) {
			if (state.siteNav[x].trackCompletion) {
		    	sortArr.push([x, state.siteNav[x].num, state.siteNav[x].done]);
			}
		}
		sortArr.sort(function(a, b) {
		    return a[1] - b[1];
		});

		// return true if there is nothing to track
		if (sortArr.length == 0) {
			return true;
		}

		// each member of the sorted array looks like [link, num, done] (sorted in order of num)
		// loop through the sorted links and make sure all the ones up till now are done
		//console.log(sortArr);
		var x = 0;
		while(sortArr[x][0] != link) {
			x++;
			if (x >= 1) {
				if (sortArr[(x-1)][2] == false) {
					alldone = false;
					break;
				}
			}
			if (x > 100) {
				break;
			}
		}

		return alldone;

	},
	allSectionsComplete: state => exclude => {
		// exclude has array of links to exclude from the sections
		// will check if everything with setCompleteion true
		// is marked done, except what's in the exclude array
		// code is similar to previousSections done
		var excludeArr = exclude ? exclude : [];

		// makesure excludeArr is all lowercase
		var lowerCaseExcluded = excludeArr.map(x => x.toLowerCase());
		//console.log(lowerCaseExcluded);


		var alldone = true;
		var sortArr = [];
		for (var x in state.siteNav) {
			if ((state.siteNav[x].trackCompletion) && (!excludeLink(x))) {
		    	sortArr.push([x, state.siteNav[x].num, state.siteNav[x].done, excludeLink(x)]);
			}
		}
		if (sortArr.length == 0) {
			return true;
		}
		sortArr.sort(function(a, b) {
		    return a[1] - b[1];
		});
		var x = 0;

		// sortArr should have everything we want
		for (var x = 0; x < sortArr.length; x++) {
			if (sortArr[(x)][2] == false) {
				alldone = false;
				break;
			}
		}

		return alldone;

		function excludeLink(link) {
			if (lowerCaseExcluded.length > 0) {
				if (lowerCaseExcluded.indexOf(link) != -1) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

}

const actions = {
	retrieveSavedMenu({commit, dispatch}, data) {
		// right now, sets sections to done and undone
		// loop through data, and unlock if locked is false
		// also loop through data and set to done if done is true

		// THIS ONLY COUNTS IF SETCOMPLETION IS SET TO TRUE

		for (var x in data) {
			if (data[x].done) {
				var menuItemKey = data[x].link.replace('/','');
				commit('setSectionToDone',menuItemKey);
			}

			if (!data[x].locked) {
				commit('unlockAnInteraction',data[x].link);
			}
		}
	},
	unlockInteraction({ commit }, link) {
		commit('unlockAnInteraction', link);
	},
	afterInteractionComplete({ commit }, fcn) { // sets a function to run at the end of each inteaction
		commit('setAfterInteractionCompleteFunction', fcn);
	},
	beforeEachInteraction({ commit }, fcn) { // sets a function to run at the start of each interaction
		commit('setBeforeEachInteractionFunction', fcn);
	},
	beforeEachPage({commit}, fcn) {
		commit('setBeforeEachPageFunction', fcn);
	},
	goToNext({commit, dispatch}) {
		// overrides the "override" function
		// used to kind of force to go to next
		state.hackyOverrideVariable = true;
		dispatch('rh_next');
	},
	goToPrev({commit, dispatch}) {
		// overrides the "override" function
		// used to kind of force to go to previous page
		state.hackyOverrideVariable = true;
		dispatch('rh_prev');
	},
	makeRoutes({ commit }, interactions) {
		// takes an array of interaction names and puts in state.routeInfo
		// returns an array of objects approperate for routes  Sasdf
		// called right at the beginning in App.vue

		// prepare the route array
		var routes = [];

		// populate and then return it
		for (var x in interactions) {

 			var component = interactions[x].component;
 			var name = '';

 			if (typeof component.name == 'undefined') {
 				alert('All components must be named!!');
 			} else {
 				name = component.name;
 			}

 			// an array to pass to the router
 			routes.push({
 				path: '/' + name.toLowerCase(),
 				component: component
 			});

 			// an array that has the siteNavInfo
 			//state.siteNav.push('/' + name.toLowerCase());
 			commit('addInteraction',
 				{
 					path: '/' + name.toLowerCase(),
 					num: x,
 					menuName: typeof interactions[x].menuName != 'undefined' ? interactions[x].menuName : '',
 					bookMark: (typeof interactions[x].bookMark != 'undefined') && (interactions[x].bookMark == true) ? true : false,
 					locked: (typeof interactions[x].locked != 'undefined') && (interactions[x].locked == true) ? true : false,
 					component: component,
 					chunk: (typeof interactions[x].chunk != 'undefined') && (interactions[x].chunk == true),
 					trackCompletion: (typeof interactions[x].trackCompletion != 'undefined') && (interactions[x].trackCompletion == true) ? true : false
 				}
 			);
		}

		router.addRoutes(routes);

		// finally set current section
		// this is called in App.vue, so just set it to the first one
		commit('setSection',interactions[0].component);

	},
	setPages({ commit }, data) {

		// similar in concept to
		// sets the pages for an interaction and adds them to siteNav
		// data contains data.name which is the name of the component calling it
		// it also contains data.pages which is an array of the compoennts to add

		commit('addPagesToComponent', data);

		// now set current component to the first position
		commit('setSectionComponent');
	},
	rh_prev({ commit, state, dispatch }, data) {
		// get currentPage and PagePosition

		// if hacky override variable is set, set to false
		state.hackyOverrideVariable = false;

		// if pagePosition isn't zero, go back one
		// if pagePosition is 0, if the num of the current interaction isn't zero, go back to the previous interactions
		// if pagePosition is 0, if the num of the current interaction is zero, go back
		if (((!state.sectionallyOverRiddenLeft) && (!state.pageOverRiddenLeft)) || (state.hackyOverrideVariable)) {
			var pos = state.pagePosition;
			if (pos > 0) {
				commit('setPagePosition', (pos-1));
				commit('setSectionComponent');
				commit('onLastPage', false);

				if (state.siteNav['/' + state.currentInteraction].num == 0) {
					commit('onFirstPage', true);
				}

			} else if (pos == 0) {
				// get num of the current interaction
				var num = parseInt(state.siteNav['/' + state.currentInteraction].num);

				if (num > 0) {
					// go to previous interaction. For now, set the page to the last page in that interaction
					// chunkifying and dechunkilizing comes later

					let newCurrentInteraction = null;

					for (var x in state.siteNav) {
						if ( parseInt(state.siteNav[x].num) == parseInt(num-1) ) {
							newCurrentInteraction = x.replace('/',''); // get rid of the starting slash
							break;
						}
					}

					// ok .. got the new current interaction, what's the last page in that interaction?
					// lets console log it for now
					// the state should already be filled out, luckly, since we're going back

					commit('setSection',newCurrentInteraction);

					var component = state.siteNav['/' + newCurrentInteraction].component;
					if (typeof component.methods.getPages != 'undefined') {
						var pages = component.methods.getPages();
						// setPages
						dispatch('setPages',{name: component.name, pages});
					}

					// if (!chunkified and Done) set page position to last page
					// if chunkified OR not done, set position to first page

					var newPagePosition = 0;
					if ((!state.siteNav['/' + newCurrentInteraction].chunk) && (state.siteNav['/' + newCurrentInteraction].done)) {
						newPagePosition = state.siteNav['/' + newCurrentInteraction].pages.length-1;
					}

					// OKAY let's commit the new page position, and the new interaction
					if (!isNaN(newPagePosition)) {
						commit('setPagePosition', (newPagePosition)); // hmm.. might have to combine these two functions??
					}

					commit('setSectionComponent');
					commit('onLastPage', false);

					// might now be on the first page. Might need to deactivate the left arrow
					if ((state.siteNav['/' + state.currentInteraction].num == 0) && (state.pagePosition == 0)) {
						commit('onFirstPage',true);
					}

				} else {
					// should set 'on first page' to true
					commit('onFirstPage',true);

				}

			} // end if position = 0
		} else {
			if (state.pageOverRiddenLeft) {
				state.pageLeftFunction() // the callback function for pages
			} else if (state.sectionallyOverRiddenLeft) {
				state.leftFunction();
			}
		}

	},
	rh_next({ commit, dispatch }, data) {

		console.log(state.siteNav);

		if (((!state.sectionallyOverRiddenRight) && (!state.pageOverRiddenRight)) || (state.hackyOverrideVariable)) {

			// if hacky override variable is set, set to false
			state.hackyOverrideVariable = false;

			// get currentPage and page Position
			// if pagePosition is less than siteNav['/' + currentInteraction].length - 1, increment pagePosition
			// else, if there's another entry inSiteNav .. set page Position to zero and change
			var pos = state.pagePosition;
			if (pos < (state.siteNav['/' + state.currentInteraction].pages.length - 1)) {
				commit('setPagePosition', (pos+1));
				commit('setSectionComponent');
				commit('onFirstPage',false); // make sure we don't think we're on first page

				// are we on last page?
				if ((parseInt(state.siteNav['/' + state.currentInteraction].num) == (state.totalInteractions-1)) && (state.pagePosition == (state.siteNav['/' + state.currentInteraction].pages.length - 1))) {
					commit('onLastPage', true);
				}

			} else {
				dispatch('goToNextSection');
			}

			/* else if (parseInt(state.siteNav['/' + state.currentInteraction].num) < (state.totalInteractions-1)) {

				// to prevent errors, vue will try to render the next component with a computed property.. but
				// the computed property will breifly be whatever it was in the last interactino, so
				// since we're changing interactions, set it to null
				commit('setSectionComponent',true); // passing true resets it

				// set pagePosition to zero
				// might want to rename pagePosition to something like, say .. subposition??
				commit('setPagePosition',0);

				// find the sitenav with the 'num' = the current one's num + 1
				// set current interaction to that
				let newNum = parseInt(state.siteNav['/' + state.currentInteraction].num) + 1;
				let newCurrentInteraction = null;

				for (var x in state.siteNav) {
					if (parseInt(state.siteNav[x].num) == newNum) {
						newCurrentInteraction = x.replace('/',''); // get rid of the starting slash
						break;
					}
				}

				commit('setSection',newCurrentInteraction); // set it to component with num being one higher
				commit('onFirstPage',false); // make sure we don't think we're on first page
				//commit('setSectionComponent'); // sets it to 'page one' or whaver

				// gotta set the current interaction to the first one
				//router.push('/' + currentURLPath);
				// are we on last page?
				// Note: this doesn't work the first time if the final component only has one page because by the time this gets evaluated, siteNav isn't populated with pages yet
				// so make sure to doule check in the function that adds pages.. in the addPagesToComponent mutation
				if ((parseInt(state.siteNav['/' + state.currentInteraction].num) == (state.totalInteractions-1)) && (state.pagePosition == (state.siteNav['/' + state.currentInteraction].pages.length - 1))) {
					commit('onLastPage', true);
				}

				//console.log('IF GOTONEXTSECTION WORKS, REPLACE ALL THIS CODE WITH A DISPATCH CALL')

			} else {
				// hmm.. the way this is laid out.. should probably never get to here
				//console.log('***************');
				//console.log('if before final page 2 we should be in the else-if above')
				//console.log(state.siteNav)
				//console.log(state.siteNav['/' + state.currentInteraction].num);
				//console.log(state.totalInteractions);
				commit('onLastPage', true); // just in case
				alert('you are at the end');
			} */
		} else {
			// either the section or the page is overridden
			// page takes precidence
			if (state.pageOverRiddenRight) {
				state.pageRightFunction() // the callback function for pages
			} else if (state.sectionallyOverRiddenRight) {
				state.rightFunction(); // a callback defined from sectionallyOverrideRight
			}
		}
	},
	goToNextSection({commit}) {
		// basically copying code from rh_next for going to next section
		// commit('setCurrentSectionToDone');

		// run the after each section function
		if ((state.afterInteractionCompleteFunction) &&  (typeof state.afterInteractionCompleteFunction == 'function')) {
			state.afterInteractionCompleteFunction();
		}

		if (parseInt(state.siteNav['/' + state.currentInteraction].num) < (state.totalInteractions-1)) {
			commit('setSectionComponent',true); // passing true resets it
			commit('setPagePosition',0);
			let newNum = parseInt(state.siteNav['/' + state.currentInteraction].num) + 1;
			let newCurrentInteraction = null;
			for (var x in state.siteNav) {
				if (parseInt(state.siteNav[x].num) == newNum) {
					newCurrentInteraction = x.replace('/',''); // get rid of the starting slash
					break;
				}
			}

			commit('setSection',newCurrentInteraction); // set it to component with num being one higher
			commit('onFirstPage',false); // make sure we don't think we're on first page
			//commit('setSectionComponent'); // sets it to 'page one' or whaver

			// gotta set the current interaction to the first one
			//router.push('/' + currentURLPath);
			// are we on last page?
			// Note: this doesn't work the first time if the final component only has one page because by the time this gets evaluated, siteNav isn't populated with pages yet
			// so make sure to doule check in the function that adds pages.. in the addPagesToComponent mutation
			if ((parseInt(state.siteNav['/' + state.currentInteraction].num) == (state.totalInteractions-1)) && (state.pagePosition == (state.siteNav['/' + state.currentInteraction].pages.length - 1))) {
				commit('onLastPage', true);
			}
		}

	},
	goToLink({commit},link) {

		//console.log('REMEBER TO GO BACK AND CHECK IF WE ARE ON FIRST OR LAST PAGE');

		var newCurrentInteraction = link.replace('/','');
		if (newCurrentInteraction == state.currentInteraction) {
			commit('setPagePosition',0);
		} else {
			commit('setSectionComponent',true);
			commit('setPagePosition',0);

			commit('setSection',newCurrentInteraction);
		}
	},
	hideNavForThisSection({commit}) {
		commit('hideNavForThisSection');
		commit('setPagePosition',0);
	},
	sectionallyOverrideRight({commit}, cb) {
		// instead of going back and forth, this function sets a variable that executes a user supplied callback function
		// in the next and prev buttons
		commit('overRideSectionRight');

		if (typeof cb != 'undefined') {
			state.rightFunction = cb;
		}
	},
	sectionallyOverrideLeft({commit}, cb) {
		// instead of going back and forth, this function sets a variable that executes a user supplied callback function
		// in the next and prev buttons
		commit('overRideSectionLeft');

		if (typeof cb != 'undefined') {
			state.leftFunction = cb;
		}
	},
	overridePageLeft({commit},cb) {
		commit('overRidePageLeft');

		if (typeof cb != 'undefined') {
			state.pageLeftFunction = cb;
		}
	},
	overridePageRight({commit},cb) {
		commit('overRidePageRight');

		if (typeof cb != 'undefined') {
			state.pageRightFunction = cb;
		}
	},
	unPageOverrideLeft({commit}) {
		commit('unOverRidePageLeft');
	},
	unPageOverrideRight({commit}) {
		commit('unOverRidePageRight');
	},
	disableNavNext({commit}) {
		commit('disableNext');
	},
	disableNavPrev({commit}) {
		commit('disablePrev');
	},
	enableNavNext({commit}) {
		commit('enableNext');
	},
	enableNavPrev({commit}) {
		commit('enablePrev');
	}
}

const mutations = {
	setBeforeEachPageFunction(state, fcn) {
		state.beforeEachPageFunction = fcn;
	},
	setAfterInteractionCompleteFunction(state, fcn) {
		state.afterInteractionCompleteFunction = fcn;
	},
	setBeforeEachInteractionFunction(state, fcn) {
		state.beforeEachInteractionFunction = fcn;
	},
	enableNavNext(state) {
		state.prevDisabled = false;
	},
	enableNavPrev(state) {
		state.nextDisabled = false;
	},
	disablePrev(state) {
		state.prevDisabled = true;
	},
	disableNext(state) {
		state.nextDisabled = true;
	},
	onFirstPage(state,val) {
		state.onFirstPage = val; // val should be true or false
		// probably should be refactored into an 'is there prev' function
	},
	onLastPage(state, val) {
		state.onLastPage = val; // val should be true or false
		// probably should be refactored into an 'is there next' function
	},
	addInteraction(state, pathData) {

		//console.log('Pathdata IS');
		//console.log(pathData);

		state.siteNav[pathData.path] = {
			pages: [],
			num: pathData.num, // just to keep track of where this object is
			done: false,
			menuName: pathData.menuName,
 			bookMark: pathData.bookMark,
 			locked: pathData.locked,
 			component: pathData.component,
 			chunk: pathData.chunk,
 			trackCompletion: pathData.trackCompletion
		};

		state.totalInteractions++;
	},
	setCurrentSectionToDone(state) {
		//console.log('SETTING SECTIONS TO DONE HAPPENS HERE!!!');
		state.siteNav['/' + state.currentInteraction].done = true;
		//console.log(state.siteNav);
		//Vue.set(state.siteNav, "['/' + state.currentInteraction].done", true);
	},
	setSectionToDone(state,section) {
		state.siteNav['/' + section].done = true;
	},
	addPagesToComponent(state, data) {

		var completeData = [];

		for (var x = 0; x < data.pages.length; x++) {
			var pageObj = {
				component: data.pages[x].component,
				bookMark: typeof data.pages[x].bookMark == 'undefined' ? false : data.pages[x].bookMark,
				slideAway: typeof data.pages[x].slideAway == 'undefined' ? false : data.pages[x].slideAway
			}

			completeData.push(pageObj);
		}

		state.siteNav['/' + data.name.toLowerCase()].pages = completeData;

		// check to see if we are on the last page..
		// because there is an edge case in the rh_next function that can't catch if it is in time before pages are added on the final component, if it only has one page
		if ((parseInt(state.siteNav['/' + state.currentInteraction].num) == (state.totalInteractions-1)) && (state.pagePosition == (state.siteNav['/' + state.currentInteraction].pages.length - 1))) {
			state.onLastPage = true;
		}
	},
	setSection(state,component) {
		// can take a compoenent or a string

		let name  = ''

		// do we have a comoponent object or a string
		if (typeof component == 'object') {
			name = component.name;
		} else {
			name = component;
		}

		// if nav had been sectionally hidden, show it
		state.sectionallyHidden = false;

		// if nav had been overridden set to false
		state.sectionallyOverRiddenRight = false;
		state.sectionallyOverRiddenLeft = false;
		// set the pages to false too
		state.pageOverRiddenLeft = false;
		state.pageOverRiddenLeft = false;

		// in case nav left or nav right has been disabled
		state.prevDisabled = false;
		state.nextDisabled = false;

		state.currentInteraction = name;
		state.currentURLPath = '/' + name;
		router.push({path: '/' + name, append: true }); // without append the WHOLD THING BREAKS when moving to server...

		// ok we should be on a new section now
		// let's run the before each interaction function
		if ((state.beforeEachInteractionFunction) &&  (typeof state.beforeEachInteractionFunction == 'function')) {
			state.beforeEachInteractionFunction();
		}


		// check if we're on first or last page too
		if ((state.siteNav['/' + state.currentInteraction].num == 0) && (state.pagePosition == 0)) {
			state.onFirstPage = true;
		} else {
			state.onFirstPage = false;
		}
		if ((parseInt(state.siteNav['/' + state.currentInteraction].num) == (state.totalInteractions-1)) && (state.pagePosition == (state.siteNav['/' + state.currentInteraction].pages.length - 1))) {
			state.onLastPage = true;
		} else {
			state.onLastPage = false;
		}

	},
	setSectionComponent(state,reset) {

		//note: each interaction has it's pages in a function that's called to put pages in siteNav
		//it's different than how interactions are added

		if (state.beforeEachPageFunction) {
			state.beforeEachPageFunction();
		}

		if ((typeof reset != 'undefined') && (reset)) {
			state.currentComponent = '';
		} else {
			let currentInteraction = state.currentInteraction;
			let pageNum = state.pagePosition;

			if (typeof state.siteNav['/' + currentInteraction].pages[pageNum] != 'undefined') {
				state.currentComponent = state.siteNav['/' + currentInteraction].pages[pageNum].component;
			} else {


				state.sectionallyOverRiddenRight = false;
				state.sectionallyOverRiddenLeft = false;
				state.pageOverRiddenLeft = null;
				state.pageOverRiddenRight = null;

				state.currentComponent = state.siteNav['/' + currentInteraction].pages[0].component;
			}
		}

	},
	unlockAnInteraction(state, link) {
		link = link.toLowerCase();
		state.siteNav[link].locked = false;
	},
	setPagePosition(state, num) {
		// reset the page overrides
		state.pageOverRiddenRight = false;
		state.pageOverRiddenLeft = false;

		state.pagePosition = num;
	},
	hideNavForThisSection(state) {
		state.sectionallyHidden = true;
	},
	overRideSectionRight(state) {
		state.sectionallyOverRiddenRight = true;
	},
	overRideSectionLeft(state) {
		state.sectionallyOverRiddenLeft = true;
	},
	overRidePageLeft(state) {
		state.pageOverRiddenLeft = true;
	},
	overRidePageRight(state) {
		state.pageOverRiddenRight = true;
	},
	unOverRidePageLeft(state) {
		state.pageOverRiddenLeft = false;
	},
	unOverRidePageRight(state) {
		//console.log('UNOVERRIDING');
		state.pageOverRiddenRight = false;
		//console.log(state);
	},
	enablePrev(state) {
		state.prevDisabled = false;
	},
	enableNext(state) {
		state.nextDisabled = false;
	}
}

export default {
	state,
	getters,
	actions,
	mutations
}
