import Vue from 'vue'
import Vuex from 'vuex'

//import { scormLMS } from './modules/scormLMS.js';
import { menu } from './modules/menu.js';
import { getData } from '../utils/http'
const json = 'data/data.json';
const config = 'data/config.json';

import rH from './modules/routeHelper';
import lmsFunctions from './modules/lmsFunctions';

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		//scormLMS,
		lmsFunctions,
		menu,
		rH
	},
	state: {
		items: null,
		config: null,
		foodserviceFound: [
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
		questionNum: 0,
		questionCorrect: [
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false
		],
		numCorrect: 0,
		glossarySection: null,
		slideAway: false,
		hovering: false,
		forceOpen: false
	},
	actions: {

	},
	mutations: {
		setItems (state, value) {
			state.items = value;
		},
		setConfig (state, value) {
			state.config = value;
		},
		checkoff(state,value) {
			//console.log(value);
			state.foodserviceFound[value] = true;
		},
		incrementQuestionNum(state,value) {
			//console.log(value);
			state.questionNum++;
		},
		correct(state,num) {
			state.questionCorrect[num] = true;
			state.numCorrect++;
		},
		resetQuix(state) {
			state.questionNum = 0;
			state.questionCorrect = [false,false,false,false,false,false,false,false,false,false];
			numCorrect = 0;
		},
		setGlossarySection(state,num) {
			state.glossarySection = num;
		},
		menuHovering(state) {
			state.hovering = true;
		},
		menuNotHovering(state) {
			state.hovering = false;
		},
		setSlideAway(state) {
			state.slideAway = true;
		},
		noSlideAway(state) {
			state.slideAway = false;
		},
		forceOpen(state, val) {
			state.forceOpen = val; // true or false
		}
	}
})
export default store
//SET DATA
const items = 'setItems'
const configData = 'setConfig'
getData(json, store, items)
getData(config, store, configData)

/*
var QueryString = function() {
		// This function is anonymous, is executed immediately and
		// the return value is assigned to QueryString!
		var query_string = {};
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i = 0; i < vars.length; i++) {
				var pair = vars[i].split("=");
				// If first entry with this name
				if (typeof query_string[pair[0]] === "undefined") {
						query_string[pair[0]] = decodeURIComponent(pair[1]);
						// If second entry with this name
				} else if (typeof query_string[pair[0]] === "string") {
						var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
						query_string[pair[0]] = arr;
						// If third or later entry with this name
				} else {
						query_string[pair[0]].push(decodeURIComponent(pair[1]));
				}
		}
		return query_string;
}();

if (QueryString.debug === "true") {
	document.addEventListener("keydown", (e) => {
		const actions = {
			ArrowRight: () => { store.commit('scormLMS/incrementProgress'), store.commit('scormLMS/locationPlus') },
			ArrowLeft: () => store.commit('scormLMS/locationMinus'),
		};
		maybe(actions[e.key]);
	})

	function maybe(fn) {
		fn ? fn() : null;
	}
}
*/