import Vue from 'vue'
import VueRouter from 'vue-router'
import first from './containers/first/first.vue'

require('./scss/styles.scss')

// Tricky tricky?? This has to be exported BEFORE APP IS IMPORTED
// store and router are injected into App.vue
// sEEMS TO WORK THIS WAY ...
export const router = new VueRouter({
    //mode: 'history',
    routes: [
    	{ path: '/', component: first }
    ]
}); 

Vue.use(VueRouter);

import App from './App.vue'

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})